from django.db import models

class evTransitStops(models.Model):
    primaryCode = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    lat = models.FloatField()
    lng = models.FloatField()

   
class evStatus(models.Model):
    primaryCode = models.CharField(max_length=50)
    timestamp = models.DateTimeField()
    vehicleType = models.IntegerField()
    statusText = models.CharField(max_length=40)
    takenPlaces = models.IntegerField()
    availablePlaces = models.IntegerField()

