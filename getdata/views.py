from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.template import RequestContext, Context, loader	
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from getdata.models import evTransitStops, evStatus
from stopids import stopIDs
import requests
import json

# Create your views here.
api_key = "Gx0A0PopRUqLvWln7fqaiQ"
headers = {'X-Api-Key': api_key }

def get_chargelocation(request):
    url = 'https://bristol.api.urbanthings.io/api/2.0/static/transitstops'
    payload = '?centerLat=51.4545501&centerLng=-2.6081068&radius=10000&stopModes=14'
    response = requests.get(url+payload, headers=headers)
    data = response.json()['data'] 
    for x in data:
        pcode = x['primaryCode']
        dc = pcode[-2:]
        d = dc[:1]
        if d =="_":
            pass
        xin = 0
        TS =evTransitStops.objects.all()
        for stop in TS:
            if stop.primaryCode == x['primaryCode']:
                xin = 1
        if xin == 0:		
            evTS = evTransitStops()
            evTS.primaryCode = x['primaryCode']
            evTS.name = x['name']
            evTS.lat = x['lat']
            evTS.lng = x['lng']
            evTS.save()
    stops = evTransitStops.objects.all().values()
    stopd = []
    stopj = []
    for stop in stops:
        stopc = []
        pc = str(stop['primaryCode'])
        print "stop: ", stop
        x = pc[-2:]
        print x
        print 'pc: ', pc
        y = x[:1]
        if y is not '_':
            url = 'https://bristol.api.urbanthings.io/api/2.0/rti/resources/status'
            payload = '?stopIDs='+ pc
            response = requests.get(url+payload, headers=headers)
            data = response.json()['data']
            for x in data:
                avail = x['availablePlaces']
            stopc.append(stop['lat'])
            stopc.append(stop['lng'])
            stopc.append(avail)
            stopd.append(stopc)
            stopj.append(stop)
    stopd = json.dumps(stopd)
    print stopd
    return render(request, 'chargelocations.html', {'stopd': stopd})

def strstopids():
    stops = ''
    i = 0
    for stop in stopIDs:
        if i == 0:
            stops = stops + stop
        else: 
            stops = stops + ', ' + stop
        i += 1
    return stops


def get_chargedetail(request, evTransitStops_id):
    ev = evTransitStops.objects.get(pk=evTransitStops_id)
    stopID = ev.primaryCode
    name = ev.name
    url = 'https://bristol.api.urbanthings.io/api/2.0/rti/resources/status'
    payload = '?stopIDs='+ stopID
    response = requests.get(url+payload, headers=headers)
    data = response.json()['data']
    cPoints = []
    for x in data:
        print 'x: ', x
        deeda = []
        deeda.append(x['lat'])
        deeda.append(x['lng'])
        deeda.append(x['availablePlaces'])
        cPoints.append(deeda)
        inuse = x['takenPlaces']
        avail = x['availablePlaces']
        noplaces = inuse + avail
        i = 0
        while i < noplaces:
            pcodes = []
            pcode = stopID + '_' + str(i)
            pcodes.append(pcode)
            i += 1
        available.append(pcodes)
    print 'available: ', available
    print 'inuse: ', inuse
    print 'avail: ', avail
    cpoints = []
    if len(available)>0:
        for x in available:
            for y in x:
                cpdata = []
                payload = '?stopIDs=' + y
                response = requests.get(url+payload, headers=headers)
                rdata = response.json()['data']
                #data = json.loads(rdata)
                print "rdata : ", rdata
                for x in rdata:
                    evS = evStatus()
                    evS.primarycode = payload
                    evS.timestamp = x['timeStamp']
                    evS.vehicleType = x['vehicleType']
                    evS.takenPlaces = x['takenPlaces']
                    evS.availablePlaces = x['availablePlaces']
                    evS.statusText = x['statusText']
                    evS.save()
            message = ''
            print 'cpoints: ', cpoints
    else: 
        message = 'There are no charge points at that location'
    return render(request, 'chargedetails.html', {'name': name, 'evs': evS, 
                                                 'inuse': inuse, 'avail': avail, 
                                                 'message': message} )

    