# /bin/bash 
NAME="charge"
DJANGODIR=/srv/chrge/
SOCKFILE=/srv/chrge/run/gunicorn.sock
USER=chargeuser
GROUP=webapps
NUM_WORKERS=3
DJANGO_SETTINGS_MODULE=charge.settings
DJANGO_WSGI_MODULE=charge.wsgi

echo "Starting $NAME as 'whoami' "

cd $DJANGODIR
soure ../venv/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

RUNDIR = $(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

exec gunicorn --log-file=- ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --log-level=debug\
  --bind=unix:$SOCKFILE